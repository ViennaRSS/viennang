//
//  VNDatabase.swift
//  ViennaNG
//
//  Created by Joshua Pore on 12/12/2015.
//  Copyright © 2015 Frisky Lentil. All rights reserved.
//

import Foundation
import FMDB

class VNDatabase {
    
    var databasePath: String {
        get {
            let directoryPath = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0]
            return "\(directoryPath)/Vienna/messages.db"
        }
    }
    
    var databaseVersion: Int {
        var version = 0
        if let dbQueue = VNDatabase().databaseQueue() {
            dbQueue.inDatabase() {
                db in
                version = Int((db?.userVersion())!)
                if version == 0 {
                    if let results = db?.executeQuery("select version from info", withArgumentsIn: nil) {
                        while results.next() {
                            version = Int(results.int(forColumn: "version"))
                        }
                    } else {
                        print("select failure: \(db?.lastErrorMessage())")
                    }

                }
            }
            dbQueue.close()
            
        } else {
            print("Unable to open database")
        }
        return version
    }
    
    
    func database() -> FMDatabase {
		let database = FMDatabase(path: databasePath)
        if !(database?.open())! {
            print("Unable to open database")
        }
        return database!
    }
    
    func databaseQueue() -> FMDatabaseQueue? {
        let databaseQueue = FMDatabaseQueue(path: databasePath)
        return databaseQueue
    }
    
    func getSubscriptionsList() -> [Subscription] {
        var subscriptionsList = [Subscription]()
        if let dbQueue = VNDatabase().databaseQueue() {
            dbQueue.inDatabase() {
                db in

                if let results = db?.executeQuery("select folder_id, foldername, type from folders", withArgumentsIn: nil) {
                    while results.next() {
                        let subscription = Subscription()
                        subscription.title = results.string(forColumn: "foldername")
                        subscription.subscriptionType = Subscription.SubscriptionType(rawValue: results.long(forColumn: "type"))!
                        subscription.folder_id = results.long(forColumn: "folder_id")
                        subscriptionsList.append(subscription)
                    }
                } else {
                    print("select failure: \(db?.lastErrorMessage())")
                }
            }
            dbQueue.close()
        } else {
            print("Unable to open database")
        }
        return subscriptionsList
    }
    
    func getPostsFor(subscription: Subscription) -> [Post] {
        var posts = [Post]()
        if let dbQueue = VNDatabase().databaseQueue() {
            dbQueue.inDatabase() {
                db in
                
                if let results = db?.executeQuery("select title from messages where folder_id=?", withArgumentsIn: [subscription.folder_id]) {
                    while results.next() {
                        let post = Post(title: results.string(forColumn: "title"))
                        posts.append(post)
                    }
                } else {
                    print("select failure: \(db?.lastErrorMessage())")
                }
            }
        } else {
            print("Unable to open database")
        }
        return posts
    }
}
