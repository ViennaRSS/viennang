//
//  SubscriptionListViewModel.swift
//  ViennaNG
//
//  Created by Joshua Pore on 3/04/2016.
//  Copyright © 2016 Frisky Lentil. All rights reserved.
//

import Foundation

class SubscriptionListViewModel {
    
    var subscriptionsList: [Subscription]
    
    var count: Int {
        get {
            return subscriptionsList.count
        }
    }

    init() {
        subscriptionsList = VNDatabase().getSubscriptionsList()
    }
    
    func subscriptionAtIndex(_ index: Int) -> Subscription {
        return subscriptionsList[index]
    }
    
    func subscriptionIsExpandable(_ subscription: Subscription) -> Bool {
        if (subscription.subscriptionType == .groupFolder) || (subscription.subscriptionType == .smartFolder) {
            return true
        }
        return false
    }
    
    func subscriptionTitle(_ subscription: Subscription) -> String {
        return subscription.title
    }

    
}
