//
//  PostDisplayViewController.swift
//  ViennaNG
//
//  Created by Joshua Pore on 9/04/2016.
//  Copyright © 2016 Frisky Lentil. All rights reserved.
//

import Cocoa

class PostDisplayViewController: NSViewController {
    
    @IBOutlet var selectedPostLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    func displaySelectedPost(_ selectedPost: Post) {
        selectedPostLabel.stringValue = selectedPost.title!
    }
    
}
