//
//  VNAMainWindowController.swift
//  ViennaNG
//
//  Created by Joshua Pore on 30/03/2016.
//  Copyright © 2016 Frisky Lentil. All rights reserved.
//

import Cocoa

class VNAMainWindowController: NSWindowController {

    @IBOutlet var feedListSplitView: NSSplitView!
    override func windowDidLoad() {
        super.windowDidLoad()
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        self.window?.contentView = feedListSplitView
    }
    
    convenience init() {
        self.init(windowNibName: "VNAMainWindow")
    }

}
