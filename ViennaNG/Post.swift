//
//  Post.swift
//  ViennaNG
//
//  Created by Joshua Pore on 10/01/2016.
//  Copyright © 2016 Frisky Lentil. All rights reserved.
//

import Foundation

class Post {
    
    var title: String?
    
    init(title: String) {
        self.title = title
    }
}