//
//  Subscription.swift
//  ViennaNG
//
//  Created by Joshua Pore on 24/12/2015.
//  Copyright © 2015 Frisky Lentil. All rights reserved.
//

import Foundation

class Subscription {
    
    enum SubscriptionType: Int {
        case smartFolder = 2
        case groupFolder = 3
        case subscription = 4
        case trashFolder = 5
        case searchResults = 6
        case cloudFolder = 7
    }
    
    var title: String
    var subscriptionType: SubscriptionType?
    var posts: [Post]
    var folder_id: Int
    
    convenience init() {
        self.init(title: "",folder_id: 0, posts: [Post]())
    }
    
    init(title: String, folder_id: Int, posts: [Post]) {
        self.title = title
        self.folder_id = folder_id
        self.posts = posts
    }
    
}
