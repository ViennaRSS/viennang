//
//  PostListViewController.swift
//  ViennaNG
//
//  Created by Joshua Pore on 3/04/2016.
//  Copyright © 2016 Frisky Lentil. All rights reserved.
//

import Cocoa

class PostListViewController: NSObject, NSTableViewDataSource, NSTableViewDelegate {

    @IBOutlet var postListView: NSTableView!
    @IBOutlet weak var postDisplayViewController: PostDisplayViewController!
    
    var selectedSubscription: Subscription {
        didSet {
            selectedSubscription.posts = VNDatabase().getPostsFor(subscription: selectedSubscription)
            postListView.reloadData()
        }
    }
    
    override init() {
        selectedSubscription = Subscription()
    }
    
    func initWith(subscription: Subscription) {
        selectedSubscription = subscription
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        print("posts count: \(selectedSubscription.posts.count)")
        return (selectedSubscription.posts.count)
    }
    func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
        //return selectedSubscription.posts![row];
        return "test cell"
    }
    
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view = tableView.make(withIdentifier: "PostCell", owner: self) as! NSTableCellView
        if let textField = view.textField {
            textField.stringValue = selectedSubscription.posts[row].title!
        }
        return view
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        let selectedPost = selectedSubscription.posts[postListView.selectedRow]
        postDisplayViewController.displaySelectedPost(selectedPost)
    }
    
    
    

}
