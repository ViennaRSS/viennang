//
//  SubscriptionListViewController.swift
//  ViennaNG
//
//  Created by Joshua Pore on 22/12/2015.
//  Copyright © 2015 Frisky Lentil. All rights reserved.
//

import Cocoa

class SubscriptionListViewController: NSObject, NSOutlineViewDataSource, NSOutlineViewDelegate {
    
    @IBOutlet var subscriptionsListView: NSOutlineView!
    @IBOutlet weak var postsListViewController: PostListViewController!
    var viewModel: SubscriptionListViewModel
    
    
    override init() {
        viewModel = SubscriptionListViewModel()
    }
    
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        return viewModel.subscriptionAtIndex(index)
    }
    
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return viewModel.subscriptionIsExpandable(item as! Subscription)
    }
    
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        return viewModel.count
    }
  
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView?  {
        let view = outlineView.make(withIdentifier: "DataCell", owner: self) as! NSTableCellView
        let subscription = item as! Subscription
        if let textField = view.textField {
            textField.stringValue = viewModel.subscriptionTitle(subscription)
        }
        return view
    }

    func outlineViewSelectionDidChange(_ notification: Notification) {
        
        if let outlineView = notification.object as? NSOutlineView {
            let selectedIndex = outlineView.selectedRow
            let selectedObject = outlineView.item(atRow: selectedIndex)
            if selectedObject is Subscription {
                print("selected subscription: \((selectedObject as! Subscription).title)")
                postsListViewController.selectedSubscription = (selectedObject as! Subscription)
            }

        }
        
    }

}
