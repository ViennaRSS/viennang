//
//  AppDelegate.swift
//  ViennaNG
//
//  Created by Joshua Pore on 9/12/2015.
//  Copyright © 2015 Frisky Lentil. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var mainWindowController: VNAMainWindowController?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        mainWindowController = VNAMainWindowController()
        mainWindowController!.showWindow(nil)
        print("DB Version: \(VNDatabase().databaseVersion)")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

