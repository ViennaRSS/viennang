# Vienna NG

Vienna NG is an attempt to substantially modernize the code and the user interface of [Vienna](http://www.vienna-rss.org), an RSS/Atom reader for Mac OS X.

As far as it is practically possible, the developers will try to reflect the state of the art available with OS X El Capitan and macOS Sierra, and they will not let compatibility with older versions hinder our work.

For instance, the user interface will probably be redrawn from scratch, using Swift 3.0 and Xcode 8. Some elements might be merged from the [legacy Vienna code](https://github.com/ViennaRSS/vienna-rss/), but only if they are considered good enough.

Vienna NG is currently at a very early stage, and has not even reached alpha quality. 

## Build Instructions
1. Clone source
2. Make sure you have Carthage installed and then run `carthage update --platform Mac` inside the vienna source folder.
3. Build xcode project as per normal
